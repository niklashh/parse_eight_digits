use criterion::{black_box, criterion_group, criterion_main, Criterion};

use parse_eight_digits::*;

pub fn bench_slow(c: &mut Criterion) {
    let digs = [0x31; 8];

    c.bench_function("slow", |b| {
        b.iter(|| {
            // Inner closure, the actual test
            for _ in 1..100 {
                black_box(parse_eight_slow(digs));
            }
        })
    });
}

pub fn bench_fast(c: &mut Criterion) {
    let digs = [0x31; 8];

    c.bench_function("fast", |b| {
        b.iter(|| {
            // Inner closure, the actual test
            for _ in 1..100 {
                black_box(parse_eight_digits_unrolled(digs));
            }
        })
    });
}

criterion_group!(benches, bench_slow, bench_fast);
criterion_main!(benches);
