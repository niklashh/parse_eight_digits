use std::ops::Shr;

pub fn parse_eight_slow(digs: [u8; 8]) -> u64 {
    let mut sum = 0;
    for (i, dig) in digs.iter().rev().enumerate() {
        assert!((&0x30..&0x40).contains(&dig));
        sum += (dig - 0x30) as u64 * 10u64.pow(i as u32);
    }
    sum
}

pub fn parse_eight_digits_unrolled(digs: [u8; 8]) -> u64 {
    let mut val = u64::from_le_bytes(digs);
    val = ((val & 0x0F0F0F0F0F0F0F0F) * (2560u64.pow(1) + 1)).shr(8);
    val = ((val & 0x00FF00FF00FF00FF) * (2560u64.pow(2) + 1)).shr(16);
    ((val & 0x0000FFFF0000FFFF) * (2560u64.pow(4) + 1)).shr(32)
}
